# Project Lucky Dice

## 1. Mô tả dự án

**Lucky Dice** là một ứng dụng web đơn giản cho phép người dùng ném xúc xắc để nhận các voucher và giải thưởng. Dự án bao gồm các API để thực hiện các thao tác liên quan đến lịch sử ném xúc xắc, lịch sử voucher và lịch sử giải thưởng của người dùng.

## 2. Công nghệ sử dụng

- **Node.js**: Nền tảng JavaScript để xây dựng ứng dụng server.
- **Express.js**: Framework cho Node.js để xây dựng các API RESTful.
- **MongoDB**: Cơ sở dữ liệu NoSQL để lưu trữ thông tin người dùng và lịch sử ném xúc xắc, voucher và giải thưởng.
- **Mongoose**: ODM (Object Data Modeling) cho MongoDB và Node.js.
- **Postman**: Công cụ để kiểm thử các API.

## 3. Giải thích code các method đã viết

### Method: `diceHandler`

Method này xử lý việc ném xúc xắc cho người dùng, lưu lịch sử ném xúc xắc và kiểm tra điều kiện để nhận voucher và giải thưởng.

- **Chuẩn bị dữ liệu**: Lấy dữ liệu từ request body và tạo một giá trị ngẫu nhiên cho xúc xắc.
- **Validate dữ liệu**: Kiểm tra các trường bắt buộc (username, firstname, lastname).
- **Tìm kiếm user**: Sử dụng `userModel` để tìm kiếm user trong cơ sở dữ liệu bằng `username`.
- **Xử lý khi user không tồn tại**: Nếu user không tồn tại, tạo user mới và lưu lịch sử ném xúc xắc.
- **Xử lý khi user tồn tại**: Nếu user tồn tại, lưu lịch sử ném xúc xắc và kiểm tra điều kiện để nhận voucher và giải thưởng.
- **Trả về kết quả**: Trả về kết quả cuối cùng với thông tin về xúc xắc, voucher và giải thưởng (nếu có).

### Method: `getAllPrizeHistories`

Method này lấy danh sách lịch sử giải thưởng của người dùng dựa trên `userId` hoặc `username`.

- **Chuẩn bị điều kiện lọc**: Lấy `userId` và `username` từ query parameters.
- **Tìm kiếm user**: Nếu có `username`, tìm kiếm user bằng `username` và lấy `_id` của user đó.
- **Lấy lịch sử giải thưởng**: Dựa trên điều kiện lọc, lấy lịch sử giải thưởng từ cơ sở dữ liệu.
- **Trả về kết quả**: Trả về danh sách lịch sử giải thưởng của người dùng.

### Method: `getAllVoucherHistories`

Method này lấy danh sách lịch sử voucher của người dùng dựa trên `userId` hoặc `username`.

- **Chuẩn bị điều kiện lọc**: Lấy `userId` và `username` từ query parameters.
- **Tìm kiếm user**: Nếu có `username`, tìm kiếm user bằng `username` và lấy `_id` của user đó.
- **Lấy lịch sử voucher**: Dựa trên điều kiện lọc, lấy lịch sử voucher từ cơ sở dữ liệu.
- **Trả về kết quả**: Trả về danh sách lịch sử voucher của người dùng.

---

Bằng cách sử dụng các method này, bạn có thể kiểm tra và quản lý lịch sử ném xúc xắc, lịch sử giải thưởng và lịch sử voucher của người dùng trong hệ thống `Lucky Dice`.
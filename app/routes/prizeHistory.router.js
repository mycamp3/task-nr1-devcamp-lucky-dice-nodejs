const express = require('express');
const router = express.Router();
const {
  createPrizeHistory,
  getAllPrizeHistories,
  getPrizeHistoryById,
  updatePrizeHistoryById,
  deletePrizeHistoryById,
  getPrizeHistoriesOfUser
} = require('../controllers/prizeHistory.controller');

// POST /prize-histories - Create a new prize history
router.post('/', createPrizeHistory);

// GET /prize-histories - Get all prize histories
router.get('/', getAllPrizeHistories);

// GET /prize-histories/:prizeHistoryId - Get a prize history by ID
router.get('/:prizeHistoryId', getPrizeHistoryById);

// PUT /prize-histories/:prizeHistoryId - Update a prize history by ID
router.put('/:prizeHistoryId', updatePrizeHistoryById);

// DELETE /prize-histories/:prizeHistoryId - Delete a prize history by ID
router.delete('/:prizeHistoryId', deletePrizeHistoryById);

module.exports = router;

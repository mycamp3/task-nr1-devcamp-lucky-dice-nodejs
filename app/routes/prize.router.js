// app/routes/prize.router.js
const express = require('express');
const router = express.Router();
const { createPrize, getAllPrizes, getPrizeById, updatePrizeById, deletePrizeById } = require('../controllers/prize.controller');

// POST /prizes - Create a new prize
router.post('/', createPrize);

// GET /prizes - Get all prizes
router.get('/', getAllPrizes);

// GET /prizes/:prizeId - Get a prize by ID
router.get('/:prizeId', getPrizeById);

// PUT /prizes/:prizeId - Update a prize by ID
router.put('/:prizeId', updatePrizeById);

// DELETE /prizes/:prizeId - Delete a prize by ID
router.delete('/:prizeId', deletePrizeById);

module.exports = router;

const express = require('express');
const router = express.Router();
const {
  createVoucherHistory,
  getAllVoucherHistories,
  getVoucherHistoryById,
  updateVoucherHistoryById,
  deleteVoucherHistoryById
} = require('../controllers/voucherHistory.controller');

// POST /voucher-histories - Create a new voucher history
router.post('/', createVoucherHistory);

// GET /voucher-histories - Get all voucher histories
router.get('/', getAllVoucherHistories);

// GET /voucher-histories/:voucherHistoryId - Get a voucher history by ID
router.get('/:voucherHistoryId', getVoucherHistoryById);

// PUT /voucher-histories/:voucherHistoryId - Update a voucher history by ID
router.put('/:voucherHistoryId', updateVoucherHistoryById);

// DELETE /voucher-histories/:voucherHistoryId - Delete a voucher history by ID
router.delete('/:voucherHistoryId', deleteVoucherHistoryById);

module.exports = router;

// app/routes/diceHistory.router.js
const express = require('express');
const router = express.Router();
const { createDiceHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById } = require('../controllers/diceHistory.controller');

// POST /dice-histories - Create a new dice history
router.post('/', createDiceHistory);

// GET /dice-histories - Get all dice histories
router.get('/', getAllDiceHistory);

// GET /dice-histories/:diceHistoryId - Get a dice history by ID
router.get('/:diceHistoryId', getDiceHistoryById);

// PUT /dice-histories/:diceHistoryId - Update a dice history by ID
router.put('/:diceHistoryId', updateDiceHistoryById);

// DELETE /dice-histories/:diceHistoryId - Delete a dice history by ID
router.delete('/:diceHistoryId', deleteDiceHistoryById);

module.exports = router;

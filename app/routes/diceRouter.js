const express = require("express");
const router = express.Router();
const { diceHandler, getAllPrizeHistories, getAllVoucherHistories, getAllDiceHistories } = require("../controllers/diceController");

router.post("/dice", diceHandler);
router.get("/prize-history", getAllPrizeHistories);
router.get("/voucher-history", getAllVoucherHistories);
router.get("/dice-history", getAllDiceHistories);
module.exports = router;

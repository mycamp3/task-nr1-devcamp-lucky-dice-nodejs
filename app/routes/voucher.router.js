// app/routes/voucher.router.js
const express = require('express');
const router = express.Router();
const { createVoucher, getAllVouchers, getVoucherById, updateVoucherById, deleteVoucherById } = require('../controllers/voucher.controller');

// POST /vouchers - Create a new voucher
router.post('/', createVoucher);

// GET /vouchers - Get all vouchers
router.get('/', getAllVouchers);

// GET /vouchers/:voucherId - Get a voucher by ID
router.get('/:voucherId', getVoucherById);

// PUT /vouchers/:voucherId - Update a voucher by ID
router.put('/:voucherId', updateVoucherById);

// DELETE /vouchers/:voucherId - Delete a voucher by ID
router.delete('/:voucherId', deleteVoucherById);

module.exports = router;

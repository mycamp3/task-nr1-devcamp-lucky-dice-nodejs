const mongoose = require('mongoose');
const User = require('../models/user.model');

// Create a new user
const createUser = async (req, res) => {
  try {
    const { username, firstname, lastname } = req.body;

    if (!username || !firstname || !lastname) {
      return res.status(400).json({ message: 'All fields are required' });
    }

    const newUser = await User.create({ username, firstname, lastname });

    res.status(201).json({ message: 'User created successfully!', data: newUser });
  } catch (error) {
    console.error('Error creating user:', error);
    res.status(500).json({ message: 'Error creating user' });
  }
};

// Get all users
const getAllUser = async (req, res) => {
  try {
    const users = await User.find();
    res.status(200).json({ message: 'Users retrieved successfully!', data: users });
  } catch (error) {
    console.error('Error retrieving users:', error);
    res.status(500).json({ message: 'Error retrieving users' });
  }
};

// Get a user by ID
const getUserById = async (req, res) => {
  try {
    const userId = req.params.userId;
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    res.status(200).json({ message: 'User retrieved successfully!', data: user });
  } catch (error) {
    console.error('Error retrieving user:', error);
    res.status(500).json({ message: 'Error retrieving user' });
  }
};

// Update a user by ID
const updateUserById = async (req, res) => {
  const { userId } = req.params;
  const updateData = req.body;

  try {
      const updatedUser = await User.findByIdAndUpdate(userId, updateData, { new: true });
      if (!updatedUser) {
          return res.status(404).json({ message: 'User not found' });
      }
      res.status(200).json({
          message: 'User updated successfully',
          data: updatedUser
      });
  } catch (error) {
      res.status(500).json({ message: 'Error updating user', error });
  }
};



// Delete a user by ID
const deleteUserById = async (req, res) => {
  try {
    const userId = req.params.userId;
    const deletedUser = await User.findByIdAndDelete(userId);

    if (!deletedUser) {
      return res.status(404).json({ message: 'User not found' });
    }

    res.status(200).json({ message: 'User deleted successfully!' });
  } catch (error) {
    console.error('Error deleting user:', error);
    res.status(500).json({ message: 'Error deleting user' });
  }
};

module.exports = { createUser, getAllUser, getUserById, updateUserById, deleteUserById };


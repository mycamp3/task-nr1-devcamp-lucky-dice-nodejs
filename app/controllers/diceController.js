const mongoose = require("mongoose");

const DiceHistory = require("../models/diceHistory.model");
const PrizeHistory = require("../models/prizeHistory.model");
const Prize = require("../models/prize.model");
const User = require("../models/user.model");
const VoucherHistory = require("../models/voucherHistory.model");
const Voucher = require("../models/voucher.model");

const diceHandler = async (req, res) => {
    try {
        const { username, firstname, lastname } = req.body;

        // Validate dữ liệu từ request body
        if (!username || !firstname || !lastname) {
            return res.status(400).json({
                status: "Error 400: Bad request",
                message: "All fields are required"
            });
        }

        // Random 1 giá trị xúc xắc bất kỳ
        const dice = Math.floor(Math.random() * 6) + 1;

        // Sử dụng userModel tìm kiếm bằng username
        let user = await User.findOne({ username });

        if (!user) {
            // Nếu user không tồn tại trong hệ thống, tạo user mới
            user = new User({
                _id: new mongoose.Types.ObjectId(),
                username,
                firstname,
                lastname
            });
            await user.save();
        }

        // Lưu lịch sử Dice
        const diceHistory = new DiceHistory({
            _id: new mongoose.Types.ObjectId(),
            user: user._id,
            dice
        });
        await diceHistory.save();

        if (dice < 3) {
            // Nếu dice < 3, không nhận được voucher và prize gì cả
            return res.status(200).json({
                dice,
                prize: null,
                voucher: null
            });
        }

        // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
        const voucherCount = await Voucher.countDocuments();
        const randomVoucher = Math.floor(Math.random() * voucherCount);
        const voucher = await Voucher.findOne().skip(randomVoucher);

        const voucherHistory = new VoucherHistory({
            _id: new mongoose.Types.ObjectId(),
            user: user._id,
            voucher: voucher._id
        });
        await voucherHistory.save();

        // Lấy 3 lần gieo xúc xắc gần nhất của user
        const last3DiceHistory = await DiceHistory.find({ user: user._id }).sort({ _id: -1 }).limit(3);

        if (last3DiceHistory.length < 3 || last3DiceHistory.some(d => d.dice <= 3)) {
            // Nếu chưa ném đủ 3 lần hoặc có lần ném nào đó <= 3, không nhận được prize
            return res.status(200).json({
                dice,
                prize: null,
                voucher
            });
        }

        // Nếu đủ điều kiện nhận giải thưởng, tiến hành lấy random 1 prize trong prize Model
        const prizeCount = await Prize.countDocuments();
        const randomPrize = Math.floor(Math.random() * prizeCount);
        const prize = await Prize.findOne().skip(randomPrize);

        const prizeHistory = new PrizeHistory({
            _id: new mongoose.Types.ObjectId(),
            user: user._id,
            prize: prize._id
        });
        await prizeHistory.save();

        return res.status(200).json({
            dice,
            prize,
            voucher
        });

    } catch (error) {
        return res.status(500).json({
            status: "Error 500: Internal server error",
            message: error.message
        });
    }
}

const getAllPrizeHistories = async (req, res) => {
    try {
        let condition = {};

        // Lấy userId từ query nếu có
        const userId = req.query.user;

        if (userId && mongoose.Types.ObjectId.isValid(userId)) {
            condition.user = userId;
        } else {
            // Nếu không có userId, kiểm tra username
            const username = req.query.username;

            if (username) {
                // Tìm user bằng username
                const user = await User.findOne({ username: username });

                if (user) {
                    condition.user = user._id;
                } else {
                    // Nếu user không tồn tại, trả về mảng rỗng
                    return res.status(200).json({
                        message: 'No prize histories found for this username',
                        data: []
                    });
                }
            }
        }

        // Lấy lịch sử giải thưởng từ cơ sở dữ liệu với điều kiện lọc
        const prizeHistories = await PrizeHistory.find(condition).populate('user').populate('prize');

        res.status(200).json({
            message: 'Get all prize histories successfully',
            data: prizeHistories
        });
    } catch (error) {
        console.error('Error getting prize histories:', error);
        res.status(500).json({ message: 'Error getting prize histories', error });
    }
};
// Get all voucher histories
const getAllVoucherHistories = async (req, res) => {
    try {
        let condition = {};

        // Lấy userId từ query nếu có
        const userId = req.query.user;

        if (userId && mongoose.Types.ObjectId.isValid(userId)) {
            condition.user = userId;
        } else {
            // Nếu không có userId, kiểm tra username
            const username = req.query.username;

            if (username) {
                // Tìm user bằng username
                const user = await User.findOne({ username: username });

                if (user) {
                    condition.user = user._id;
                } else {
                    // Nếu user không tồn tại, trả về mảng rỗng
                    return res.status(200).json({
                        message: 'No voucher histories found for this username',
                        data: []
                    });
                }
            }
        }

        // Lấy lịch sử voucher từ cơ sở dữ liệu với điều kiện lọc
        const voucherHistories = await VoucherHistory.find(condition).populate('user').populate('voucher');

        res.status(200).json({
            message: 'Get all voucher histories successfully',
            data: voucherHistories
        });
    } catch (error) {
        console.error('Error getting voucher histories:', error);
        res.status(500).json({ message: 'Error getting voucher histories', error });
    }
};
// Get all voucher histories
const getAllDiceHistories = async (req, res) => {
    try {
        let condition = {};
  
        // Lấy userId từ query nếu có
        const userId = req.query.user;
  
        if (userId && mongoose.Types.ObjectId.isValid(userId)) {
            condition.user = userId;
        } else {
            // Nếu không có userId, kiểm tra username
            const username = req.query.username;
  
            if (username) {
                // Tìm user bằng username
                const user = await User.findOne({ username: username });
  
                if (user) {
                    condition.user = user._id;
                } else {
                    // Nếu user không tồn tại, trả về mảng rỗng
                    return res.status(200).json({
                        message: 'No dice histories found for this username',
                        data: []
                    });
                }
            }
        }
  
        // Lấy lịch sử voucher từ cơ sở dữ liệu với điều kiện lọc
        const voucherHistories = await DiceHistory.find(condition).populate('user').populate('dice');
  
        res.status(200).json({
            message: 'Get all dice histories successfully',
            data: voucherHistories
        });
    } catch (error) {
        console.error('Error getting dice histories:', error);
        res.status(500).json({ message: 'Error getting dice histories', error });
    }
  };
module.exports = {
    diceHandler,
    getAllPrizeHistories,
    getAllVoucherHistories,
    getAllDiceHistories
}

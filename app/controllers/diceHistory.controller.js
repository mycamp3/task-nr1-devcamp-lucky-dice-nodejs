// app/controllers/diceHistory.controller.js
const mongoose = require('mongoose');
const DiceHistory = require('../models/diceHistory.model');
const User = require('../models/user.model');

// Function to create dice history
const createDiceHistory = async (req, res) => {
    const { userId, dice } = req.body;

    // Validate userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: 'Invalid userId'
        });
    }

    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({
                message: 'User not found'
            });
        }

        // Validate dice
        if (typeof dice !== 'number' || dice < 1 || dice > 6) {
            return res.status(400).json({ message: 'Dice must be a number between 1 and 6' });
        }

        const newDiceHistory = new DiceHistory({
            user: user._id,  // user._id là ObjectId của user tìm thấy
            dice: dice
        });

        const savedDiceHistory = await newDiceHistory.save();

        // Log the newDiceHistory object for debugging
        console.log('New Dice History:', newDiceHistory);

        res.status(201).json({
            message: "Dice History created successfully",
            data: savedDiceHistory
        });
    } catch (error) {
        res.status(500).json({ message: 'Error creating dice history', error });
    }
};

const getAllDiceHistory = async (req, res) => {
    try {
        // Step 2: Initialize an empty condition object
        let condition = {};

        // Step 3: Get the user ID from query parameters
        const user = req.query.user;

        // Step 4: If user ID exists, add it to the condition object
        if (user) {
            condition.user = user;
        }

        // Fetch dice histories based on condition and populate user field
        const diceHistories = await DiceHistory.find(condition).populate('user');

        // Respond with the fetched dice histories
        res.status(200).json(diceHistories);
    } catch (error) {
        // Handle any errors that occur during the process
        res.status(500).json({ message: 'Error getting dice histories', error });
    }
};

// Function to delete dice history by ID
const deleteDiceHistoryById = async (req, res) => {
    const diceHistoryId = req.params.diceHistoryId;

    try {
        const deletedDiceHistory = await DiceHistory.findByIdAndDelete(diceHistoryId);
        if (!deletedDiceHistory) {
            return res.status(404).json({ message: 'Dice history not found' });
        }
        res.status(200).json({
            message: 'Dice history deleted successfully',
            data: deletedDiceHistory
        });
    } catch (error) {
        res.status(500).json({ message: 'Error deleting dice history', error });
    }
};


// Function to get dice history by ID
const getDiceHistoryById = async (req, res) => {
    const diceHistoryId = req.params.diceHistoryId;

    // Validate diceHistoryId
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({ message: 'Invalid dice history ID' });
    }

    try {
        const diceHistory = await DiceHistory.findById(diceHistoryId).populate('user');
        if (!diceHistory) {
            return res.status(404).json({ message: 'Dice history not found' });
        }
        res.status(200).json(diceHistory);
    } catch (error) {
        res.status(500).json({ message: 'Error getting dice history by ID', error });
    }
};

const updateDiceHistoryById = async (req, res) => {
    const diceHistoryId = req.params.diceHistoryId;
    const { dice } = req.body;

    // Validate diceHistoryId
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({ message: 'Invalid dice history ID' });
    }

    try {
        const updatedDiceHistory = await DiceHistory.findByIdAndUpdate(
            diceHistoryId,
            { $set: { dice } },
            { new: true }
        );

        if (!updatedDiceHistory) {
            return res.status(404).json({ message: 'Dice history not found' });
        }

        res.status(200).json({
            message: 'Dice history updated successfully',
            data: updatedDiceHistory
        });
    } catch (error) {
        res.status(500).json({ message: 'Error updating dice history', error });
    }
};
module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    deleteDiceHistoryById,
    updateDiceHistoryById
};

// app/controllers/voucher.controller.js
const Voucher = require('../models/voucher.model');

// Function to create voucher
const createVoucher = async (req, res) => {
    const { code, discount, note } = req.body;

    // Validate code
    if (!code) {
        return res.status(400).json({ message: 'Invalid code' });
    }

    // Validate discount
    if (typeof discount !== 'number' || discount <= 0) {
        return res.status(400).json({ message: 'Discount must be a positive number' });
    }

    try {
        const newVoucher = new Voucher({ code, discount, note });
        const savedVoucher = await newVoucher.save();
        res.status(201).json({
            message: "Voucher created successfully",
            data: savedVoucher
        });
    } catch (error) {
        res.status(500).json({ message: 'Error creating voucher', error });
    }
};

// Function to get all vouchers
const getAllVouchers = async (req, res) => {
    try {
        const vouchers = await Voucher.find();
        res.status(200).json(vouchers);
    } catch (error) {
        res.status(500).json({ message: 'Error getting vouchers', error });
    }
};

// Function to get a voucher by ID
const getVoucherById = async (req, res) => {
    const { voucherId } = req.params;

    try {
        const voucher = await Voucher.findById(voucherId);
        if (!voucher) {
            return res.status(404).json({ message: 'Voucher not found' });
        }
        res.status(200).json(voucher);
    } catch (error) {
        res.status(500).json({ message: 'Error getting voucher', error });
    }
};

// Function to update a voucher by ID
const updateVoucherById = async (req, res) => {
    const { voucherId } = req.params;
    const updateData = req.body;

    try {
        const updatedVoucher = await Voucher.findByIdAndUpdate(voucherId, updateData, { new: true });
        if (!updatedVoucher) {
            return res.status(404).json({ message: 'Voucher not found' });
        }
        res.status(200).json({
            message: 'Voucher updated successfully',
            data: updatedVoucher
        });
    } catch (error) {
        res.status(500).json({ message: 'Error updating voucher', error });
    }
};

// Function to delete a voucher by ID
const deleteVoucherById = async (req, res) => {
    const { voucherId } = req.params;

    try {
        const deletedVoucher = await Voucher.findByIdAndDelete(voucherId);
        if (!deletedVoucher) {
            return res.status(404).json({ message: 'Voucher not found' });
        }
        res.status(200).json({ message: 'Voucher deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: 'Error deleting voucher', error });
    }
};

module.exports = {
    createVoucher,
    getAllVouchers,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
};

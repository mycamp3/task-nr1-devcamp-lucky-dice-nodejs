// app/controllers/prize.controller.js
const Prize = require('../models/prize.model');

// Function to create prize
const createPrize = async (req, res) => {
    const { name, description } = req.body;

    // Validate name
    if (!name) {
        return res.status(400).json({ message: 'Invalid name' });
    }

    // Validate description
    if (description && typeof description !== 'string') {
        return res.status(400).json({ message: 'Description must be a string' });
    }

    try {
        const newPrize = new Prize({ name, description });
        const savedPrize = await newPrize.save();
        res.status(201).json({
            message: "Prize created successfully",
            data: savedPrize
        });
    } catch (error) {
        res.status(500).json({ message: 'Error creating prize', error });
    }
};

// Function to get all prizes
const getAllPrizes = async (req, res) => {
    try {
        const prizes = await Prize.find();
        res.status(200).json(prizes);
    } catch (error) {
        res.status(500).json({ message: 'Error getting prizes', error });
    }
};

// Function to get a prize by ID
const getPrizeById = async (req, res) => {
    const { prizeId } = req.params;

    try {
        const prize = await Prize.findById(prizeId);
        if (!prize) {
            return res.status(404).json({ message: 'Prize not found' });
        }
        res.status(200).json(prize);
    } catch (error) {
        res.status(500).json({ message: 'Error getting prize', error });
    }
};

// Function to update a prize by ID
const updatePrizeById = async (req, res) => {
    const { prizeId } = req.params;
    const updateData = req.body;

    try {
        const updatedPrize = await Prize.findByIdAndUpdate(prizeId, updateData, { new: true });
        if (!updatedPrize) {
            return res.status(404).json({ message: 'Prize not found' });
        }
        res.status(200).json({
            message: 'Prize updated successfully',
            data: updatedPrize
        });
    } catch (error) {
        res.status(500).json({ message: 'Error updating prize', error });
    }
};

// Function to delete a prize by ID
const deletePrizeById = async (req, res) => {
    const { prizeId } = req.params;

    try {
        const deletedPrize = await Prize.findByIdAndDelete(prizeId);
        if (!deletedPrize) {
            return res.status(404).json({ message: 'Prize not found' });
        }
        res.status(200).json({ message: 'Prize deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: 'Error deleting prize', error });
    }
};

module.exports = {
    createPrize,
    getAllPrizes,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
};

const mongoose = require('mongoose');
const PrizeHistory = require('../models/prizeHistory.model');
const User = require('../models/user.model');
const Prize = require('../models/prize.model');
// Create a new prize history
const createPrizeHistory = async (req, res) => {
    const { userId, prizeId } = req.body;

    // Validate userId and prizeId
    if (!mongoose.Types.ObjectId.isValid(userId) || !mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: 'Invalid userId or prizeId'
        });
    }

    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({
                message: 'User not found'
            });
        }

        const prize = await Prize.findById(prizeId);
        if (!prize) {
            return res.status(404).json({
                message: 'Prize not found'
            });
        }

        const newPrizeHistory = new PrizeHistory({
            user: user._id,
            prize: prize._id
        });

        const savedPrizeHistory = await newPrizeHistory.save();

        console.log('New Prize History:', newPrizeHistory);

        res.status(201).json({
            message: "Prize History created successfully",
            data: savedPrizeHistory
        });
    } catch (error) {
        res.status(500).json({ message: 'Error creating prize history', error });
    }
};

// Get all prize histories
const getAllPrizeHistories = async (req, res) => {
    try {
        let condition = {};

        // Lấy userId từ query nếu có
        const userId = req.query.user;

        if (userId && mongoose.Types.ObjectId.isValid(userId)) {
            condition.user = userId;
        } else {
            // Nếu không có userId, kiểm tra username
            const username = req.query.username;

            if (username) {
                // Tìm user bằng username
                const user = await User.findOne({ username: username });

                if (user) {
                    condition.user = user._id;
                } else {
                    // Nếu user không tồn tại, trả về mảng rỗng
                    return res.status(200).json({
                        message: 'No prize histories found for this username',
                        data: []
                    });
                }
            }
        }

        // Lấy lịch sử giải thưởng từ cơ sở dữ liệu với điều kiện lọc
        const prizeHistories = await PrizeHistory.find(condition).populate('user').populate('prize');

        res.status(200).json({
            message: 'Get all prize histories successfully',
            data: prizeHistories
        });
    } catch (error) {
        console.error('Error getting prize histories:', error);
        res.status(500).json({ message: 'Error getting prize histories', error });
    }
};

// Get a prize history by ID
const getPrizeHistoryById = async (req, res) => {
    try {
        const prizeHistoryId = req.params.prizeHistoryId;
        const prizeHistory = await PrizeHistory.findById(prizeHistoryId).populate('user').populate('prize');

        if (!prizeHistory) {
            return res.status(404).json({ message: 'Prize history not found' });
        }

        res.status(200).json({ message: 'Prize history retrieved successfully!', data: prizeHistory });
    } catch (error) {
        console.error('Error retrieving prize history:', error);
        res.status(500).json({ message: 'Error retrieving prize history', error });
    }
};

// Update a prize history by ID
const updatePrizeHistoryById = async (req, res) => {
    const { prizeHistoryId } = req.params;
    const updateData = req.body;

    try {
        const updatedPrizeHistory = await PrizeHistory.findByIdAndUpdate(prizeHistoryId, updateData, { new: true });

        if (!updatedPrizeHistory) {
            return res.status(404).json({ message: 'Prize history not found' });
        }

        res.status(200).json({
            message: 'Prize history updated successfully',
            data: updatedPrizeHistory
        });
    } catch (error) {
        res.status(500).json({ message: 'Error updating prize history', error });
    }
};

// Delete a prize history by ID
const deletePrizeHistoryById = async (req, res) => {
    try {
        const prizeHistoryId = req.params.prizeHistoryId;
        const deletedPrizeHistory = await PrizeHistory.findByIdAndDelete(prizeHistoryId);

        if (!deletedPrizeHistory) {
            return res.status(404).json({ message: 'Prize history not found' });
        }

        res.status(200).json({ message: 'Prize history deleted successfully!' });
    } catch (error) {
        console.error('Error deleting prize history:', error);
        res.status(500).json({ message: 'Error deleting prize history', error });
    }
};

const getPrizeHistoriesOfUser = (request, response) => {
    const username = request.query.username;

    if (!username) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Username is required"
        });
    }

    // Sử dụng userModel để tìm kiếm user bằng username
    User.findOne({ username: username }, (errorFindUser, userExist) => {
        if (errorFindUser) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindUser.message
            });
        }

        if (!userExist) {
            // Nếu user không tồn tại, trả về mảng rỗng
            return response.status(200).json([]);
        }

        // Nếu user tồn tại, lấy danh sách PrizeHistory theo user
        PrizeHistory.find({ user: userExist._id }, (errorFindPrizeHistory, prizeHistoryList) => {
            if (errorFindPrizeHistory) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: errorFindPrizeHistory.message
                });
            }

            // Trả về danh sách PrizeHistory
            return response.status(200).json(prizeHistoryList);
        });
    });
};

module.exports = {
    createPrizeHistory,
    getAllPrizeHistories,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById,
    getPrizeHistoriesOfUser
};

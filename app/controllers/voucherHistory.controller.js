const mongoose = require('mongoose');
const User = require('../models/user.model');
const Voucher = require('../models/voucher.model');
const VoucherHistory = require('../models/voucherHistory.model');
// Create a new voucher history
const createVoucherHistory = async (req, res) => {
    const { userId, voucherId } = req.body;

    // Validate userId and voucherId
    if (!mongoose.Types.ObjectId.isValid(userId) || !mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: 'Invalid userId or voucherId'
        });
    }
    if (!mongoose.Types.ObjectId.isValid(voucherId) || !mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: 'Invalid voucherId or voucherId'
        });
    }
    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({
                message: 'User not found'
            });
        }

        const voucher = await Voucher.findById(voucherId);
        if (!voucher) {
            return res.status(404).json({
                message: 'Voucher not found'
            });
        }

        const newVoucherHistory = new VoucherHistory({
            user: user._id,
            voucher: voucher._id
        });

        const savedVoucherHistory = await newVoucherHistory.save();

        console.log('New Voucher History:', newVoucherHistory);

        res.status(201).json({
            message: "Voucher History created successfully",
            data: savedVoucherHistory
        });
    } catch (error) {
        res.status(500).json({ message: 'Error creating voucher history', error });
    }
};

// Get all voucher histories
const getAllVoucherHistories = async (req, res) => {
  try {
      let condition = {};

      // Lấy userId từ query nếu có
      const userId = req.query.user;

      if (userId && mongoose.Types.ObjectId.isValid(userId)) {
          condition.user = userId;
      } else {
          // Nếu không có userId, kiểm tra username
          const username = req.query.username;

          if (username) {
              // Tìm user bằng username
              const user = await User.findOne({ username: username });

              if (user) {
                  condition.user = user._id;
              } else {
                  // Nếu user không tồn tại, trả về mảng rỗng
                  return res.status(200).json({
                      message: 'No voucher histories found for this username',
                      data: []
                  });
              }
          }
      }

      // Lấy lịch sử voucher từ cơ sở dữ liệu với điều kiện lọc
      const voucherHistories = await VoucherHistory.find(condition).populate('user').populate('voucher');

      res.status(200).json({
          message: 'Get all voucher histories successfully',
          data: voucherHistories
      });
  } catch (error) {
      console.error('Error getting voucher histories:', error);
      res.status(500).json({ message: 'Error getting voucher histories', error });
  }
};

// Get a voucher history by ID
const getVoucherHistoryById = async (req, res) => {
  try {
    const voucherHistoryId = req.params.voucherHistoryId;
    const voucherHistory = await VoucherHistory.findById(voucherHistoryId).populate('user').populate('voucher');

    if (!voucherHistory) {
      return res.status(404).json({ message: 'Voucher history not found' });
    }

    res.status(200).json({ message: 'Voucher history retrieved successfully!', data: voucherHistory });
  } catch (error) {
    console.error('Error retrieving voucher history:', error);
    res.status(500).json({ message: 'Error retrieving voucher history', error });
  }
};

// Update a voucher history by ID
const updateVoucherHistoryById = async (req, res) => {
  const { voucherHistoryId } = req.params;
  const updateData = req.body;

  try {
    const updatedVoucherHistory = await VoucherHistory.findByIdAndUpdate(voucherHistoryId, updateData, { new: true });

    if (!updatedVoucherHistory) {
      return res.status(404).json({ message: 'Voucher history not found' });
    }

    res.status(200).json({
      message: 'Voucher history updated successfully',
      data: updatedVoucherHistory
    });
  } catch (error) {
    res.status(500).json({ message: 'Error updating voucher history', error });
  }
};

// Delete a voucher history by ID
const deleteVoucherHistoryById = async (req, res) => {
  try {
    const voucherHistoryId = req.params.voucherHistoryId;
    const deletedVoucherHistory = await VoucherHistory.findByIdAndDelete(voucherHistoryId);

    if (!deletedVoucherHistory) {
      return res.status(404).json({ message: 'Voucher history not found' });
    }

    res.status(200).json({ message: 'Voucher history deleted successfully!' });
  } catch (error) {
    console.error('Error deleting voucher history:', error);
    res.status(500).json({ message: 'Error deleting voucher history', error });
  }
};

module.exports = {
  createVoucherHistory,
  getAllVoucherHistories,
  getVoucherHistoryById,
  updateVoucherHistoryById,
  deleteVoucherHistoryById
};

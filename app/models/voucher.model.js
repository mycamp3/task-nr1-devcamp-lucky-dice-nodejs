// app/models/voucher.model.js
const mongoose = require('mongoose');
const { Schema } = mongoose;

// Define the Voucher schema
const voucherSchema = new Schema({
    code: {
        type: String,
        unique: true,
        required: true
    },
    discount: {
        type: Number,
        required: true
    },
    note: {
        type: String,
    }
}, {
    timestamps: true // This will add `createdAt` and `updatedAt`
});

// Create the Voucher model
const Voucher = mongoose.model('Voucher', voucherSchema);

module.exports = Voucher;

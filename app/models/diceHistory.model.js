const mongoose = require('mongoose');
const { Schema } = mongoose;

// Define the DiceHistory schema
const diceHistorySchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  dice: {
    type: Number,
    required: true
  }
}, {
  timestamps: true // This will add `createdAt` and `updatedAt`
});

// Create the DiceHistory model
const DiceHistory = mongoose.model('DiceHistory', diceHistorySchema);

module.exports = DiceHistory;

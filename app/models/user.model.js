const mongoose = require('mongoose');
const { Schema } = mongoose;

// Define the User schema
const userSchema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  }
}, {
  timestamps: true // This will add `createdAt` and `updatedAt`
});

// Create the User model
const User = mongoose.model('User', userSchema);

module.exports = User;

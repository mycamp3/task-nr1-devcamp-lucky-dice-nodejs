
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const voucherHistorySchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    voucher: {
      type: Schema.Types.ObjectId,
      ref: 'Voucher',
      required: true
    }
  },
  { timestamps: true }
);

const VoucherHistory = mongoose.model('VoucherHistory', voucherHistorySchema);

module.exports = VoucherHistory;

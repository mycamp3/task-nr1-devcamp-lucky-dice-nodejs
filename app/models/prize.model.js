// app/models/prize.model.js
const mongoose = require('mongoose');
const { Schema } = mongoose;

// Define the Prize schema
const prizeSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String,
    }
}, {
    timestamps: true // This will add `createdAt` and `updatedAt`
});

// Create the Prize model
const Prize = mongoose.model('Prize', prizeSchema);

module.exports = Prize;

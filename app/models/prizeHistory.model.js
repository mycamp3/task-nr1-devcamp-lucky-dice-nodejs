
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const prizeHistorySchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    prize: {
      type: Schema.Types.ObjectId,
      ref: 'Prize',
      required: true
    }
  },
  { timestamps: true }
);

const PrizeHistory = mongoose.model('PrizeHistory', prizeHistorySchema);

module.exports = PrizeHistory;

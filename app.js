const express = require('express');
const app = express();
const path = require('path');
const port = 8000;
const mongoose = require('mongoose');
const userRouter = require('./app/routes/user.router');
const diceHistoryRouter = require('./app/routes/diceHistory.router');
const prizeRouter = require('./app/routes/prize.router');
const voucherRouter = require('./app/routes/voucher.router');
const voucherHistoryRouter = require('./app/routes/voucherHistory.router');
const prizeHistoryRouter = require('./app/routes/prizeHistory.router');
const diceRoute = require("./app/routes/diceRouter");

//middleware
app.use(express.json());
app.use(express.static('views'));

app.use((req, res, next) => {
    const currentDate = new Date();
    console.log(`Current Date and Time: ${currentDate}`);
    next();
});

// Middleware để in URL của request ra console
app.use((req, res, next) => {
    console.log(`Requested method: ${req.method}`);
    next();
});

// khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/devcamp-devcamp-lucky-dice-nodejs")
    .then(() => {
        console.log("MongoDB connected");
    })
    .catch((err) => {
        console.log(err);
    });

// Routes
app.get('/', (req, res) => {
    res.send('Welcome to Devcamp Lucky Dice Nodejs!');
});

// định nghĩa route cho trang chủ
app.get('/', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const indexPath = path.join(__dirname, './views/index.html');
    res.sendFile(indexPath);
})
app.get('/restAPI', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const indexPath = path.join(__dirname, './views/sample.restAPI.calls.newDice.dicevoucherprizeHistory.v2.2.html');
    res.sendFile(indexPath);
})

app.use('/users', userRouter);
app.use('/dice_histories', diceHistoryRouter);
app.use('/prizes', prizeRouter);
app.use('/vouchers', voucherRouter);
app.use('/voucher_histories', voucherHistoryRouter);
app.use('/prize_histories', prizeHistoryRouter);
app.use("/devcamp-lucky-dice", diceRoute);

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
})